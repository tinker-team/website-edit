# Changelog

All notable changes to Website edit are documented in this file.
The sections should follow the order
`Packaging`, `Added`, `Changed`, `Fixed` and `Removed`.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/).


## [Unreleased]

### Added

  * Initial experimental release

[Unreleased]: <https://salsa.debian.org/tinker-team/website-edit/-/commits/main/>
