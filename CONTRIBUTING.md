# Contributing to Alacritty

Thank you for your interest in contributing to Web-edit!


## Feature Requests

Feature requests should be reported by email
to <mailto:dr@jones.dk>.


## Bug Reports

Bug reports should be reported by email
to <mailto:dr@jones.dk>.


## Patches / Pull Requests

Bug reports should either be attached an email
to <mailto:dr@jones.dk>,
or published in a public git repo somewhere
and the URI shared as an email
to <mailto:dr@jones.dk>.


### Testing

To make sure no regressions were introduced,
all tests should be run before sending a pull request.
The following command can be run:

```
cargo test
```

Additionally if there's any functionality included
which would lend itself to additional testing,
new tests should be added,
e.g. using the `#[test]` annotation.


### Documentation

Code should be documented where appropriate.
The existing code can be used as a guidance here
and the general `rustfmt` rules can be followed for formatting.

Changes compared to the latest crate release
which have a direct effect on the user
(opposed to things like code refactorings or documentation/tests)
additionally need to be documented in the `CHANGELOG.md`.
The existing entries should be used as a style guideline.
The change log should be used
to document changes from a user-perspective,
instead of explaining the technical background
(like commit messages).
More information about the change log format
can be found [here](https://keepachangelog.com).


### Style

All changes should conform to the rustfmt guidelines.
Please install rustfmt and format all code using `cargo fmt`.

Unless otherwise specified,
this project follows the Rust compiler's style guidelines:
<https://rust-lang.github.io/api-guidelines>

Documentation comments should be fully punctuated
with a trailing period,
except headlines.
Regular non-document comments need no trailing period.


# Contact

If there are any outstanding questions
about contributing to Alacritty,
feel free to contact the authors directly.

As a more immediate and direct form of communication,
the authors usually hang out at the Tinker team IRC channel
(`#tinker` on OFTC.net).
