# Pending tasks

Tasks are tied to several subprojects,
[Service][#Service], [filters][#Filters], and [client][#Client],
that might each be completely independent.
or perhaps all share some library parts.

A minimal viable product
is a service that can save as file.
This can be used with existing clients,
and filtering can instead be (more crudely) done
using pre- and post-processing scripts.


## Service: core

Create a minimal [Micropub] proxy service,
using [indieweb] and [git2].

Implement only MUSTs in [Microsub server spec]
covering only  post [entry] of kind *[article]*
(i.e. simply fail if POST contains any other data).

No processing of POST content
(but maybe log at level trace for debugging purposes).

[Micropub]: <https://www.w3.org/TR/micropub/>
  "Micropub - open standard for simple web content editing"

[indieweb]: <https://crates.io/crates/indieweb>
  "indieweb - rust crate implementing utilities for working with the IndieWeb"

[Microsub server spec]: <https://www.w3.org/TR/micropub/#servers>
  "requirements for server implementations of Micropub"

[entry]: <https://indieweb.org/Microsub-spec#entry>
  "definition of Microsub post entry types"

[article]: <https://www.w3.org/TR/micropub/#new-article-with-html>
  "article - Micropub item containing html (e.g. a blog post)"


### Service: save as file

Implement simple file-based storage.

When option `content-dir` is set,
store content of each POST as a file,
auto-creating relative directories as needed
(or return error e.g. if file already exists).

Path set in option `content-dir` must exist and be writable.
Refuse to run if that is not the case at startup,
and die if it happens while running.


### Service: track file with git

Extend (or fork?) feature `files` to optionally track with git
using crate [git2].

When option `git-branch` is set,
add and commit created file to git
(or return error e.g. on merge conflict).

When option `git-remote` is set,
also push git changes to that git remote
(or return error).

Refuse to run if `git-remote` is set but not `git-branch`.

[git2]: <https://crates.io/crates/git2>
  "git2 - rust bindings to libgit2 for interoperating with git repositories"


### Service: content filter hooks

Implement optional content filter,
as feature `filter` enabled by default
(a reason to disable might be security concerns).

When option `post-filter` is set,
execute that string as shell command for each POST,
passing POST content as stdin
and metadata as environment variables prefixed with `WEBSITE_EDIT_`,
and capture stdout from command
and replace metadata with `WEBSITE_EDIT_`-prefixed environment variables
(return error on non-zero exit-code from command execution).


### Service: edit existing file

Cover Micropub scopes *read* and *update*.

When requested to read dir,
return list of existing files in git.

When requested to read file,
return content of file in git.

When requested to update file,
replace existing file in git
(fail if unable to do that)

Extend feature `filter`
to process option `get-filter` as well,
for each GET request reading contents of an existing file.


### Service: handle media files

Implement optional [Media Endpoint],
as feature `media` enabled by default
(a reason to disable might be size constraints).

Advertise `media-endpoint` handling.

Accept POST of media file,
storing same as for article file.

Respond to GET query,
listing stored media files.

[Media Endpoint]: <https://www.w3.org/TR/micropub/#media-endpoint>
  "Media Endpoint - Micropub file upload handling"


### Service: track media with git-annex

Cover accepting upload of new media to local git using git-annex
(fail if `git annex add ...` fails).


### Service: add source download endpoint

Implement endpoint to download source code
using crate [include-repo].

[include-repo]: <https://crates.io/crates/include-repo>


### Service: handle alternative storage backends

Implement abstraction to support multiple storage backends
using crate [vfs], [vfs-clgit], [async-vfs], [rivia-vfs], or [opendal].

[vfs]: <https://crates.io/crates/vfs>

[vfs-clgit]: <https://crates.io/crates/vfs-clgit>

[async-vfs]: <https://crates.io/crates/async-vfs>

[rivia-vfs]: <https://crates.io/crates/rivia-vfs>

[opendal]: <https://crates.io/crates/opendal>


## Filters

A filter transform content and metadata
from one format to another.


### Filters: commonmark

Implement command-line tools `to-commonmark` and `from-commonmark`
usable as filters.

Convert POST content from html to CommonMark,
maybe using [html2md].

Convert GET content from CommonMark to html.

[html2md]: <https://crates.io/crates/html2md>
  "html2md - convert simple html documents into markdown"


### Filters: semantic-commonmark

Implement command-line tools `to-semantic-commonmark`
and `from-semantic-commonmark`
usable as filters.

Convert html with [microformats] to [CommonMark-RDF].

Convert [CommonMark-RDF] to html with [microformats].

[CommonMark-RDF]: <https://source.jones.dk/commonmark-rdf-spec/tree/README.md?h=master>
  "CommonMark-RDF - draft spec for CommonMark with semantic hints"

[microformats]: <https://indieweb.org/microformats2>
  "microformats - Indieweb semantic markup"


## Client

Several [Micropub clients] exist.

The most promising is [Indigenous].
Unfortunately the desktop flavor is implemented on top of Electron,
which is unlikely to ever be packaged in Debian.

[Micropub clients]: <https://indieweb.org/Micropub/Clients>
  "Overview of Micropub client implementations"

[Indigenous]: <https://indigenous.marksuth.dev/>
  "Indigenous - app to interact with Micropub and Microsub services"


### Client: core

Create a minimal desktop client,
using [indieweb] and [egui].

[indieweb]: <https://crates.io/crates/indieweb>
  "Rust crate implementing utilities for working with the IndieWeb"

[egui]: <https://crates.io/crates/eframe>
  "egui framework - write GUI apps that compiles to web and/or natively"


### Client: commonmark editor

Implement optional preview
when editing CommonMark content,
maybe using [egui_commonmark].

[egui_commonmark]: <https://crates.io/crates/egui_commonmark>
  "egui_commonmark - Commonmark viewer for egui"
