// SPDX-FileCopyrightText: 2022  Jonas Smedegaard <dr@jones.dk>
// SPDX-FileCopyrightText: 2022  Thierry Marianne <thierry.marianne@pm.me>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

/*!
# access to files in a filesystem directory

This [module] contains routines
to interact with files in a directory.
*/

use miette::{IntoDiagnostic, Report};
use std::{fs::File, io::prelude::*, path};

#[cfg(test)]
use std::path::Path;

/**
FIXME:
* [ ] ~~Configure the target directory~~ Consider the current directory
* [ ] Extract and remove property `content`
* [ ] Create a file, and save content
* [ ] Return Path
 */
pub fn write_to_disk(file_stem: String, content: String) -> Result<(), Report> {
	let file_name = file_stem + ".txt";

	let file_path = ["./", file_name.as_str()].join(path::MAIN_SEPARATOR.to_string().as_str());

	let mut file = File::create(file_path).into_diagnostic()?;
	file.write_all(content.as_bytes()).into_diagnostic()?;

	Ok(())
}

#[test]
fn it_writes_a_file_to_disk() -> Result<(), Report> {
	write_to_disk(String::from("file"), String::from("what"))?;

	assert!(Path::exists(Path::new("./file.txt")));

	Ok(())
}
