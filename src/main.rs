// SPDX-FileCopyrightText: 2022  Jonas Smedegaard <dr@jones.dk>
// SPDX-FileCopyrightText: 2022  Thierry Marianne <thierry.marianne@pm.me>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

/*!
# Bridges between user-friendly applications and lightweight workflows

This [crate] contains routines and tools
enabling user-friendly applications
to join [shell pipeline] workflows.

In an ideal world
our user-facing tools would be optimized
both for intuitive use
and for efficient computing
with lightweight resource consumption.

However,
lightweight and efficient data processing
often involves command-line interfaces
which can be intimidating to non-technical users,
and user-friendly tools are often not very lightweight.

Website-edit helps separate concerns,
by bridging well-designed user-facing applications
and lighweight efficient [shell pipeline] workflows.

[crate]: https://doc.rust-lang.org/stable/rust-by-example/crates.html
  "Crate - compilation unit in Rust"

[shell pipeline]: <https://en.wikipedia.org/wiki/Pipeline_(Unix)>
  "shell pipeline - mechanism for efficient tooling on UNIX-like systems"
*/
use trillium::Handler;
use trillium_logger::Logger;
use trillium_router::Router;

mod micropub;
mod storage;

/**
HTTP endpoint

This function provides an HTTP endpoint
that parses and responds to [Micropub] requests.

[Micropub]: <https://en.wikipedia.org/wiki/Micropub_(protocol)>
  "Micropub - open standard for simple web content editing"
*/
fn application() -> impl Handler {
	(
		Logger::new(),
		Router::new().post("/micropub", micropub::endpoint),
	)
}

/**
# CLI tool bridging endpoints and shell pipeline workflows

This command.line tool implements a [12-Factor App]
that listens on a network socket,
accesses local files,
and logs activities to `stdout`.

[12-Factor App]: <https://12factor.net/>
  "Twelve-Factor App - methodology for building software-as-a-service applications"


# Settings

* `RUST_LOG`: emit logs of at most `error`/`warn`/`info`/`debug`/`trace` severity
  (default: `info`)
* `HOST`: bind to the specified UNIX/TCP socket
  (default: `loopback`)
* `PORT`: use the specified port when binding to a TCP socket
  (default: `8080`)

# Examples

Listen on the IPv4 loopback interface at port 3001:

```shell
HOST=127.0.0.1 PORT=3001 cargo run
```

Emit debug logs:

```shell
RUST_LOG=debug cargo run
```
*/
fn main() {
	dotenv::dotenv().ok();
	env_logger::init();
	trillium_smol::run(application());
}
