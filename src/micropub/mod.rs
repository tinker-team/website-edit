// SPDX-FileCopyrightText: 2022  Jonas Smedegaard <dr@jones.dk>
// SPDX-FileCopyrightText: 2022  Thierry Marianne <thierry.marianne@pm.me>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

/*!
# [Micropub]-related routines

This [module] contains routines
to parse and respond to [Micropub] requests.

[Micropub]: <https://www.w3.org/TR/micropub/>
  "Micropub - open standard for simple web content editing"

[module]: <https://doc.rust-lang.org/stable/rust-by-example/mod.html>
  "Crate module - collection of Rust functions, structs, traits, and more"
*/

use trillium::{conn_try, Conn, KnownHeaderName, Status};

#[cfg(test)]
use trillium_testing::prelude::*;

mod article;

/**
# HTTP endpoint implementing a [Micropub server]

FIXME:
* [ ] Parse POST data as indieweb::algorithms::Properties
* [ ] Extract and remove property `content`
* [ ] save content into file, with properties as YAML header
* [ ] Return location, reflecting Path to succesfully saved file

[Micropub server]: <https://www.w3.org/TR/micropub/#server>
  "Micropub server - HTTP endpoint that can create/edit/delete posts"
*/
pub async fn endpoint(mut conn: Conn) -> Conn {
	let request_body = conn_try!(conn.request_body_string().await, conn);
	if request_body.is_empty() {
		conn.with_status(Status::NotAcceptable)
			.with_body("unacceptable!")
			.halt()
	} else {
		let boundary = conn
			.headers()
			.get_str(KnownHeaderName::ContentType)
			.and_then(|ct| multer::parse_boundary(ct).ok());
		match boundary {
			None => conn
				.with_status(Status::NotAcceptable)
				.with_body("unacceptable!")
				.halt(),
			Some(_i) => conn
				.with_status(Status::Accepted)
				.with_header("location", "https://example.org/example-article"),
		}
	}
}

#[test]
fn posting_create_request_is_accepted() {
	let application = super::application();
	assert_response!(
		post("/micropub")
			.with_request_body("--X-BOUNDARY\r\nContent-Disposition: form-data; name=\"h\"\r\n\r\nentry\r\n--X-BOUNDARY\r\nContent-Disposition: form-data; name=\"post-status\"\r\n\r\ndraft\r\n--X-BOUNDARY\r\nContent-Disposition: form-data; name=\"content\"\r\n\r\ntest\r\n--X-BOUNDARY\r\nContent-Disposition: form-data; name=\"name\"\r\n\r\nTrying this now\r\n--X-BOUNDARY\r\nContent-Disposition: form-data; name=\"published\"\r\n\r\n2022-04-23T08:58\r\n--X-BOUNDARY--\r\n")
			.with_request_header(KnownHeaderName::ContentType, "multipart/form-data; boundary=X-BOUNDARY")
			.on(&application),
		Status::Accepted,
		"",
		"content-length" => "0",
		"location" => "https://example.org/example-article",
	);
}

#[test]
fn posting_nothing_is_rejected() {
	let application = super::application();
	assert_response!(
		post("/micropub")
			.with_request_header(KnownHeaderName::ContentType, "multipart/form-data; boundary=X-BOUNDARY")
			.on(&application),
		Status::NotAcceptable,
		"unacceptable!",
		"content-length" => "13",
	);
}

#[test]
fn posting_without_form_data_header_is_rejected() {
	let application = super::application();
	assert_response!(
		post("/micropub")
			.with_request_body("--X-BOUNDARY\r\nContent-Disposition: form-data; name=\"h\"\r\n\r\nentry\r\n--X-BOUNDARY\r\nContent-Disposition: form-data; name=\"post-status\"\r\n\r\ndraft\r\n--X-BOUNDARY\r\nContent-Disposition: form-data; name=\"content\"\r\n\r\ntest\r\n--X-BOUNDARY\r\nContent-Disposition: form-data; name=\"name\"\r\n\r\nTrying this now\r\n--X-BOUNDARY\r\nContent-Disposition: form-data; name=\"published\"\r\n\r\n2022-04-23T08:58\r\n--X-BOUNDARY--\r\n")
			.on(&application),
		Status::NotAcceptable,
		"unacceptable!",
		"content-length" => "13",
	);
}
