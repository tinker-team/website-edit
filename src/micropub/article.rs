// SPDX-FileCopyrightText: 2022  Jonas Smedegaard <dr@jones.dk>
// SPDX-FileCopyrightText: 2022  Thierry Marianne <thierry.marianne@pm.me>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

/*!
# [Micropub article]-related routines

This [module] contains routines
to process [Micropub article] objects.

[Micropub article]: <https://indieweb.org/article>
  "Micropub article - post with non-trivial structure"

[module]: <https://doc.rust-lang.org/stable/rust-by-example/mod.html>
  "Crate module - collection of Rust functions, structs, traits, and more"
*/

use miette::{IntoDiagnostic, Report};
use multer::Multipart;
use serde_json::{value::Value, Map};

/**
FIXME:
* [ ] Parse POST data as indieweb::algorithms::Properties
 */
pub async fn parse_request<'a>(multipart: &mut Multipart<'a>) -> Result<Value, Report> {
	let mut response = Map::new();
	let mut properties = Map::new();

	while let Some(field) = multipart.next_field().await.into_diagnostic()? {
		let field_name = field.name().unwrap().to_string();
		let field_text = field.text().await.unwrap().to_string();

		if field_name == "h" {
			response.insert(
				String::from("type"),
				serde_json::to_value(["h-entry"]).unwrap(),
			);
		} else {
			properties.insert(field_name, serde_json::to_value(field_text).unwrap());
		}
	}

	response.insert(
		String::from("post-type"),
		serde_json::to_value(["article"]).unwrap(),
	);
	response.insert(
		String::from("properties"),
		serde_json::to_value(properties).unwrap(),
	);

	Ok(serde_json::to_value(&response).unwrap())
}
