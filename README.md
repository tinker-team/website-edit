# Website edit

> Bridges between user-friendly applications and lightweight workflows

Website-edit is a set of routines and tools
enabling user-friendly applications
to join [shell pipeline] workflows
using standards-based interfaces and conventions.

Ideally, your software applications...
* provide an intuitive user interface
* do exactly what you need
* are efficient and lightweight

Often you need to make tradeoffs between those qualities,
however.

Flexible, efficient and lightweight tools
often has a command-line interface[^cli],
which can be intimidating to non-technical users.

Well-designed user-facing applications
are often opinionated in what they do and how to do it.
Some of them however support standards-based interfaces
to connect with external tooling.

Website-edit helps separate concerns,
by bridging well-designed user-facing applications
and lighweight efficient [shell pipeline] workflows
using open standards like [Micropub].

Currently this project contains a single tool:
A web service providing access
from [Micropub] clients
to a filesystem directory.

[^cli]: An essential concept in the design of UNIX-style systems
(e.g. linux)
is the [DOTADIW] principle about separations of concerns,
implemented through "piping" output of one command to input of another
in a text-based "shell" environment.
This principle helps (technical) users
to flexibly compose efficient toolchains,
and helps avoid [software bloat].

[shell pipeline]: <https://en.wikipedia.org/wiki/Pipeline_(Unix)>
  "shell pipeline - mechanism for efficient tooling on UNIX-like systems"

[DOTADIW]: <https://en.wikipedia.org/wiki/Unix_philosophy#Do_One_Thing_and_Do_It_Well>
  "Do One Thing And Do It Well - core UNIX design principle"

[software bloat]: <https://en.wikipedia.org/wiki/Software_bloat>
  "software bloat - applications becoming bigger and slower over time"

[Micropub]: <https://www.w3.org/TR/micropub/>
  "Micropub - open standard for simple web content editing"


## Configuration

Copy the configuration file template and update its entries per your need.

```shell
make copy-configuration-file
```

- `HOST` - the host where the backend will be available from, and
- `PORT` - the port which the backend will be listening on


# Deployment

```shell
cargo run
```


## Background

The authors design and host websites and other services,
for friends or paying customers.
They desire sustainable self-hosted solutions,
but also want site owners to have direct control over their content.

Many static site generators exists,
but efficient ones like [hugo] lack user-friendly editing.

Several [Micropub services] exist.
Unfortunately the few [Free Software Micropub services] that exist
are tied to specific website rendering projects.

[Hugo]: <https://en.wikipedia.org/wiki/Hugo_(software)>
  "Hugo - static site generator written in Go"

[Micropub services]: <https://indieweb.org/Micropub/Servers>
  "Overview of Micropub server implementations"

[Free Software Micropub services]: <https://indieweb.org/Micropub/Servers#Implementation_status>
  "Comparison of features covered in Free Software Micropub implementations"


# License

Copyright (C) 2022  Jonas Smedegaard <dr@jones.dk>
Copyright (C) 2022  Thierry Marianne <thierry.marianne@pm.me>

This program is free software:
you can redistribute it and/or modify it
under the terms of the GNU Affero General Public License
as published by the Free Software Foundation,
either version 3 of the License,
or (at your option) any later version.
