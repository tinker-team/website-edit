# How to test this proxy service with curl?

Assuming curl is available in command-line.

Run a development server:

```shell
make serve-development
```

## Create a post of kind article

### Tasks

- [x] Validate an [article (`h-entry`)](https://microformats.org/wiki/h-entry)
  by using [QueryResponse from indieweb crate](https://docs.rs/indieweb/0.1.7/indieweb/micropub/enum.QueryResponse.html)
- [x] Return a Create HTTP response
- [ ] Add [Location header](https://www.w3.org/TR/micropub/#response-p-4) to response
- [ ] Remove submitted content fields (testing)

```shell
curl --include -F post-status=draft -F content=test -F name='Trying this now' -F published=2022-04-23T08:58 http://127.0.0.1:3001/micropub
```

```shell
HTTP/1.1 201 Created
content-type: application/json
x-content-type-options: nosniff
access-control-allow-origin: *
access-control-allow-methods: *
access-control-allow-headers: *
access-control-expose-headers: *
content-length: 160
date: Tue, 03 May 2022 16:10:52 GMT

{
  "post-type": "article",
  "type": [
    "h-entry"
  ],
  "properties": {
    "published": [
      "2022-04-23T08:58"
    ],
    "content": [
      "test"
    ],
    "name": [
      "Trying this now"
    ],
    "post-status": [
      "draft"
    ]
  }
}
```
