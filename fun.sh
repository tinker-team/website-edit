#!/bin/sh
# SPDX-FileCopyrightText: 2022  Jonas Smedegaard <dr@jones.dk>
# SPDX-FileCopyrightText: 2022  Thierry Marianne <thierry.marianne@pm.me>
#
# SPDX-License-Identifier: AGPL-3.0-or-later

copy_configuration_file() {
  [ -e .env ] || cp --verbose .env.dist .env
}

stop_service() {
  pkill -INT --full --list-name --list-full website-edit || true
}

tail_logs() {
  if command -v jq /dev/null;  then
    trap 'kill $(jobs -p)' EXIT
    tail -F log/backend.log | jq &
    tail -F log/backend.error.log
  else
    tail -F log/backend.log log/backend.error.log
  fi
}

serve() {
  mkdir --parents log
  stop_service >> log/backend.log 2>> log/backend.error.log
  target/"${1:-debug}"/website-edit >> log/backend.log 2>> log/backend.error.log
}
